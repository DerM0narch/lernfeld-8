""" Log-Dateien werden erstellt und ausgegeben """
import psutil
import logging
import json
import os
import os.path


parent_folder = os.getcwd()
logfile = os.path.join(parent_folder, "logfile.log")

json_folder = os.getcwd()
jsonfile = os.path.join(json_folder, "threshold.json")

logging.basicConfig(
    level=logging.DEBUG,
    format="{asctime} {levelname:<8} {message}",
    style='{',
    filename=logfile,
    filemode='a'
)


def log():
    """logging der Statistiken
    """
    with open(jsonfile) as f:
        data = json.load(f)

    used_ram = psutil.virtual_memory().percent
    disk_free = psutil.disk_usage('/').free // (1024.0 ** 3)
    disk_used = psutil.disk_usage('/').used // (1024.0 ** 3)
    disk_total = psutil.disk_usage('/').total // (1024.0 ** 3)
    process_count = len(psutil.pids())

    if used_ram < data['available_ram_warning']:
        logging.info(f"RAM: {used_ram}% genutzt")
    elif used_ram < data['available_ram_acceptable']:
        logging.warning(f"RAM: {used_ram}% genutzt")
    else:
        logging.critical(f"RAM: {used_ram}% genutzt")

    if len(psutil.pids()) > data['process_count_critical']:
        logging.critical(f"Prozesse: {process_count} aktiv")
    elif len(psutil.pids()) > data['process_count_warning']:
        logging.warning(f"Prozesse: {process_count} aktiv")
    else:
        logging.info(f"Prozesse: {process_count} aktiv")

    if disk_free < disk_used:
        logging.warning(f"Speicher: {disk_used} GiB / {disk_total} GiB")
    else:
        logging.info(f"Speicher: {disk_used} GiB / {disk_total} GiB")
