import psutil
import time
import platform
import GPUtil
from logger import log
import json
from rich import print
import os

used_ram = psutil.virtual_memory().percent
disk_free = psutil.disk_usage('/').free // (1024.0 ** 3)
disk_used = psutil.disk_usage('/').used // (1024.0 ** 3)
disk_total = psutil.disk_usage('/').total // (1024.0 ** 3)
process_count = len(psutil.pids())
available_ram = round(psutil.virtual_memory().available * 100 / psutil.virtual_memory().total, 2)


user_system = platform.uname()


def warns():
    """Warnung und Farben für Systemstatistiken"""

    json_folder = os.getcwd()
    jsonfile = os.path.join(json_folder, "threshold.json")

    with open(jsonfile) as f:
        data = json.load(f)

    global available_ram_color
    global used_ram_color
    global process_count_color
    global disk_color

    if available_ram > data['available_ram_acceptable']:
        available_ram_color = "white"
        used_ram_color = "white"
    elif available_ram > data['available_ram_warning']:
        available_ram_color = "yellow"
        used_ram_color = "yellow"
    else:
        available_ram_color = "red"
        used_ram_color = "red"

    if len(psutil.pids()) > data['process_count_critical']:
        process_count_color = "red"
    elif len(psutil.pids()) > data['process_count_warning']:
        process_count_color = "yellow"
    else:
        process_count_color = "white"

    if disk_free < disk_used:
        disk_color = "yellow"
    else:
        disk_color = "white"


def outputConsole():
    """gibt Statistiken in Konsole aus"""
    warns()
    log()
    print("Aktualisiert:", (time.strftime("%d.%m.%Y um %H:%M:%S")))
    print("[bold]-----------------------------------------------------------------------[/]")
    print("[bold]                            Statistik[/]")
    print("[bold]-----------------------------------------------------------------------[/]")
    print(f"Verfügbarer RAM: [{available_ram_color}] {str(available_ram)} % [/]")
    print(f"genutzter RAM:[{used_ram_color}] {str(used_ram)} % [/]")
    print(f"freier Speicher:[{disk_color}] {str(disk_free)} GiB [/]")
    print(f"genutzter Speicher:[{disk_color}] {str(disk_used)} GiB [/]")
    print(f"Gesamtspeicher:[{disk_color}] {str(disk_total)} GiB [/]")
    print(f"Anzahl Prozesse:[{process_count_color}] {process_count} [/]")
    print("[bold]-----------------------------------------------------------------------[/]")
    print("[bold]                               GPU [/]")
    print("[bold]-----------------------------------------------------------------------[/]")
    gpus = GPUtil.getGPUs()
    for gpu in gpus:
        print(f"ID: {gpu.id}, Name: {gpu.name}")
        print(f"Load: {gpu.load * 100}%")
        print(f"Free Mem: {gpu.memoryFree}MB")
        print(f"Used Mem: {gpu.memoryUsed}MB")
        print(f"Total Mem: {gpu.memoryTotal}MB")
        print(f"Temperature: {gpu.temperature} °C")
        print("[bold]-----------------------------------------------------------------------[/]")
        print("[bold]                             Festplatte [/]")
        print("[bold]-----------------------------------------------------------------------[/]")
    partitions = psutil.disk_partitions()
    for p in partitions:
        print(f"Device: {p.device}")
        print(f"\tMountpoint: {p.mountpoint}")
        print(f"\tFile system type: {p.fstype}")
        try:
            partition_usage = psutil.disk_usage(p.mountpoint)
        except PermissionError:
            continue
        print(f"Total Size: {partition_usage.total}")
        print(f"Used: {partition_usage.used}")
        print(f"Free: {partition_usage.free}")
        print(f"Percentage: {partition_usage.percent}%")
    disk_io = psutil.disk_io_counters()
    print(f"Read since boot: {disk_io.read_bytes}")
    print(f"Written since boot: {disk_io.write_bytes}")
    print("[bold]-----------------------------------------------------------------------[/]")
    print("[bold]                             Done [/]")
