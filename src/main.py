"""Hauptprogramm / Executable"""

import tkinter
# from tkinter import *
import os
import os.path
from time import sleep
import platform
from rich import print
import getpass
import socket
from consoles import outputConsole
import json
import psutil
from errorsmtp import ErrorProcess, ErrorRAM

# Breaker für Emails setzen
error_process = ErrorProcess()
error_ram = ErrorRAM()
ram_breaker = 0
process_breaker = 0

# JSON Config öffnen
json_folder = os.getcwd()
jsonfile = os.path.join(json_folder, "threshold.json")
with open(jsonfile) as f:
    data = json.load(f)

print("--------------------\nMonitoring-Tool v1.0\n--------------------")
print("Eingeloggter Benutzer:", getpass.getuser())

# Schriftgröße angezeigt in Pixel
FONT_SIZE = 17

fenster = tkinter.Tk()

# Fenster-Titel
fenster.title("Monitoring-Tool v1.0")

# Fenster-Größe
fenster.minsize(width=600, height=410)
fenster.resizable(False, False)

# Fenster-Farbe
fenster.configure(bg="gray20")

# Systemdaten
user_system = platform.uname()

label1 = tkinter.Label(
    fenster,
    text="System-Informationen",
    font=("Helvetica 18"),
    fg="SteelBlue2",
    bg="grey20")
label1.pack(pady=10)

label2 = tkinter.Label(fenster, text="System: " +
                       user_system.system, font=(None, -
                                                 FONT_SIZE), fg="Ivory3", bg="grey20")
label2.pack()

label3 = tkinter.Label(fenster, text="Computername: " +
                       user_system.node, font=(None, -
                                               FONT_SIZE), fg="Ivory3", bg="grey20")
label3.pack()

label4 = tkinter.Label(fenster, text="IPv4-Adresse: " +
                       socket.gethostbyname(socket.gethostname()), font=(None, -
                                                                         FONT_SIZE), fg="Ivory3", bg="grey20")
label4.pack()

label5 = tkinter.Label(fenster, text="Version: " +
                       user_system.version, font=(None, -
                                                  FONT_SIZE), fg="Ivory3", bg="grey20")
label5.pack()

label6 = tkinter.Label(fenster, text="Maschine: " +
                       user_system.machine, font=(None, -
                                                  FONT_SIZE), fg="Ivory3", bg="grey20")
label6.pack()

label7 = tkinter.Label(fenster, text="Prozessor: " +
                       user_system.processor, font=(None, -
                                                    FONT_SIZE), fg="Ivory3", bg="grey20")
label7.pack()

label8 = tkinter.Label(fenster, text="", font=(None, 10), bg="grey20")
label8.pack()
label8.place(x=362, y=215)

label9 = tkinter.Label(
    fenster,
    text="IT-Solutions GmbH",
    font=(
        None,
        9),
    fg="Ivory3",
    bg="grey20")
label9.pack()
label9.place(x=5, y=388)


label10 = tkinter.Label(
    fenster,
    text="Es gibt keine Warnungen",
    font=(
        None,
        9),
    fg="Ivory3",
    bg="grey20")
label10.pack()
label10.place(x=452, y=388)

# -----------------------------------------------------------------------------------------------------------------------


def warninglabel():
    """setzt ein Warnlabel unter rechts im GUI"""
    global ram_test
    global process_test

    disk_free = psutil.disk_usage('/').free // (1024.0 ** 3)
    disk_used = psutil.disk_usage('/').used // (1024.0 ** 3)
    available_ram = round(psutil.virtual_memory().available * 100 / psutil.virtual_memory().total, 2)

    if available_ram > data['available_ram_acceptable']:
        ram_test = 0  # OK
    elif available_ram > data['available_ram_warning']:
        ram_test = 1  # WARNING
    else:
        ram_test = 2  # CRITICAL

    if len(psutil.pids()) > data['process_count_critical']:
        process_test = 2
    elif len(psutil.pids()) > data['process_count_warning']:
        process_test = 1
    else:
        process_test = 0

    if disk_free < disk_used:
        disk_test = 1
    else:
        disk_test = 0

    if ram_test == 2 or process_test == 2:
        label10["text"] = "         Es gibt Warnungen!"
        label10["fg"] = "red"
    elif ram_test == 1 or process_test == 1 or disk_test == 1:
        label10["text"] = "         Es gibt Warnungen!"
        label10["fg"] = "yellow"
    else:
        label10["text"] = "    Es gibt keine Warnungen"
        label10["fg"] = "Ivory3"


def sendwarnings():
    """sendet E-Mail sollte ein kritischer Wert erreicht werden und wartet
    bis zur Behebung des Fehler, bevor erneut eine gesendet werden kann"""

    global ram_breaker
    global process_breaker

    if ram_test == 2 and ram_breaker != 1:
        error_ram.send()
        ram_breaker = 1
    elif ram_test == 0:
        ram_breaker = 0
    if process_test == 2 and process_breaker != 1:
        error_process.send()
        process_breaker = 1
    elif process_test == 0:
        process_breaker = 0
# -----------------------------------------------------------------------------------------------------------------------


def button_clear():
    """leert die Konsole"""
    os.system('cls' if os.name == 'nt' else 'clear')
    button2["state"] = tkinter.DISABLED
    print("Der Konsoleninhalt wurde gelöscht.")


def button_log():
    """Pfad der Log-Datei anzeigen
    """
    if (os.path.isfile("logfile.log")):
        print("Die Logdatei befindet sich auf:", os.getcwd())
        os.startfile("logfile.log")

    else:
        print("Es existiert keine Logdatei.")


def button_log_del():
    """Log-Datei leeren
    """
    if (os.path.isfile('logfile.log')):
        file = open("logfile.log", "r+")
        file.truncate(0)
        file.close()
        button2["state"] = tkinter.DISABLED
        button3["state"] = tkinter.DISABLED
        button4["state"] = tkinter.DISABLED
        print("Die Logdatei wurde erfolgreich gelöscht.")

    else:
        print("Es existiert keine Logdatei.")


def button_exit():
    """Programm beenden
    """
    print("Das Programm wird beendet...")
    sleep(1)
    exit()


def start():
    """Überwachung starten
    """
    global running
    running = True


def stop():
    """Überwachung beenden
    """
    global running
    running = False


def button_switch():
    """Button aktivieren, wenn Programm läuft
    """
    if(button1["text"] == "Monitoring starten"):
        button2["state"] = tkinter.NORMAL
        button3["state"] = tkinter.NORMAL
        button4["state"] = tkinter.NORMAL
    else:
        button2["state"] = tkinter.NORMAL
        button3["state"] = tkinter.NORMAL
        button4["state"] = tkinter.NORMAL


def button_text():
    """Texte der Buttons ändern
    """
    if(button1["text"] == "Monitoring starten"):
        button1["text"] = "Monitoring stoppen"
        button1["command"] = lambda: [button_text(), stop()]
        label8["text"] = "wird ausgeführt..."
        label8["fg"] = "lime green"
        print("Das Monitoring wurde gestartet...")

    elif (button1["text"] == "Monitoring stoppen"):
        button1["text"] = "Monitoring starten"
        button1["command"] = lambda: [
            button_text(),
            button_switch(),
            start(),
            console_start()]
        label8["text"] = "gestoppt"
        label8["fg"] = "red"

        print("Das Monitoring wurde gestoppt.")

# E-Mail Konfiguration


def window_mail():
    """Fenster zur Bearbeitung der E-Mail
    """
    mailwindow = tkinter.Toplevel()
    mailwindow.minsize(width=250, height=170)
    # Fenster-Farbe
    mailwindow.configure(bg="gray20")
    mailwindow.title("E-Mail konfigurationen")
    mailwindow.resizable(False, False)

    tkinter.Label(
        mailwindow,
        text="E-Mail-Adresse",
        font="None, 10 bold",
        fg="Ivory3",
        bg="grey20").pack()
    mail_adresse = tkinter.Entry(mailwindow, width=26)
    mail_adresse.insert(0, "testmail_lernfeld_8@gmx.de")
    mail_adresse.pack()

    tkinter.Label(
        mailwindow,
        text="SMTP",
        font="None, 10 bold",
        fg="Ivory3",
        bg="grey20").pack()
    eingabe_host = tkinter.Entry(mailwindow, width=14)
    eingabe_host.insert(0, "mail.gmx.net")
    eingabe_host.pack()

    tkinter.Label(
        mailwindow,
        text="Port",
        font="None, 10 bold",
        fg="Ivory3",
        bg="grey20").pack()
    eingabe_port = tkinter.Entry(mailwindow, width=12)
    eingabe_port.insert(0, "587")
    eingabe_port.pack()

    # button5["state"] = tkinter.DISABLED

    tkinter.Button(
        mailwindow,
        text="Schließen",
        bg="grey70",
        fg="black",
        command=mailwindow.destroy).pack(
        pady=10)


# BUTTONS
button1 = tkinter.Button(
    fenster,
    text='Monitoring starten',
    bg="grey70",
    fg="black",
    command=lambda: [
        button_text(),
        button_switch(),
        start(),
        console_start()])
button1.pack(pady=10)


button2 = tkinter.Button(
    fenster,
    text="Konsoleninhalt löschen",
    bg="grey70",
    fg="black",
    state=tkinter.DISABLED,
    command=button_clear)
button2.pack(pady=5)


button3 = tkinter.Button(
    fenster,
    text="Logdatei öffnen",
    bg="grey70",
    fg="black",
    state=tkinter.DISABLED,
    command=button_log)
button3.pack()
button3.place(x=197, y=295)


button4 = tkinter.Button(
    fenster,
    text="Logdatei löschen",
    bg="grey70",
    fg="black",
    state=tkinter.DISABLED,
    command=button_log_del)
button4.pack()
button4.place(x=303, y=295)


button5 = tkinter.Button(
    fenster,
    text="E-Mail konfigurationen",
    bg="grey70",
    fg="black",
    command=window_mail)
button5.pack()
button5.place(x=235, y=335)


button6 = tkinter.Button(
    fenster,
    text="Programm beenden",
    bg="grey70",
    fg="black",
    command=button_exit)
button6.pack()
button6.place(x=243, y=374)


def console_start():
    """Konsole starten
    """
    if running:
        outputConsole()
        warninglabel()
        sendwarnings()
        fenster.after(5000, console_start)


if __name__ == "__main__":
    fenster.mainloop()
