"""Testvorlage: ob psutil richtige Daten ausgibt"""

import psutil

used_ram = psutil.virtual_memory().percent
disk_free = psutil.disk_usage('/').free // (1024.0 ** 3)
disk_used = psutil.disk_usage('/').used // (1024.0 ** 3)
disk_total = psutil.disk_usage('/').total // (1024.0 ** 3)
process_count = len(psutil.pids())
available_ram = round(psutil.virtual_memory().available * 100 / psutil.virtual_memory().total, 2)
