""" E-Mail Warn-System """
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

USER = "testmail_lernfeld_8@gmx.de"
PASSWORD = "lernfeld8"
EMAIL = "testmail_lernfeld_8@gmx.de"
RECEIVER_MAIL = "testmail_lernfeld_8@gmx.de"
HOST = "mail.gmx.net"
PORT = 587


# Warnmail ist nur als Template gedacht und soll nicht per import verwendet werden
class Warnmail():
    """Template für ErrorMails
    """

    def __init__(self):
        """initiate Class
        """
        self.user = USER
        self.password = PASSWORD
        self.email = EMAIL
        self.receiver = RECEIVER_MAIL
        self.host = HOST
        self.port = PORT
        self.subject = ""
        self.mailtext = ""

    def send(self):
        """Senden einer E-Mail
        """
        mimemsg = MIMEMultipart()
        mimemsg['From'] = self.email
        mimemsg['To'] = self.receiver
        mimemsg['Subject'] = self.subject
        mimemsg.attach(MIMEText(self.mailtext, 'plain'))
        connection = smtplib.SMTP(host=self.host, port=self.port)
        connection.starttls()
        connection.login(self.user, self.password)
        connection.send_message(mimemsg)
        connection.quit()


class ErrorRAM(Warnmail):
    """Error Warnung für RAM

    Args:
        Warnmail (class): Superclass
    """

    def __init__(self):
        """initiate Class from Warnmail
        """
        super().__init__()
        self.subject = "Error 101: RAM Werte überschritten"
        self.mailtext = "Der RAM wert wurde überschritten. Bitte überprüfen Sie die Prozesse etc."


class ErrorProcess(Warnmail):
    """Error Warnung für Prozesse

    Args:
        Warnmail (class): Superclass
    """

    def __init__(self):
        """initiate Class from Warnmail
        """
        super().__init__()
        self.subject = "Error 102: Es laufen zu viele Prozesse"
        self.mailtext = "Es werden zu viele Prozesse gleichzeitig ausgeführt. Bitte schließen Sie ggf. nicht benötigte Anwendungen."
