"""Testdatei für 'variables.py'"""
import src.variables as variables
import psutil


def test_Variables():
    assert variables.available_ram == round(psutil.virtual_memory().available * 100 / psutil.virtual_memory().total, 2)
    assert variables.used_ram == psutil.virtual_memory().percent
    assert variables.disk_free ==  psutil.disk_usage('/').free // (1024.0 ** 3)
    assert variables.disk_used == psutil.disk_usage('/').used // (1024.0 ** 3)
    assert variables.disk_total == psutil.disk_usage('/').total // (1024.0 ** 3)
    assert variables.process_count == len(psutil.pids())