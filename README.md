## Wiki zum nachlesen [Link](https://gitlab.com/DerM0narch/lernfeld-8/-/wikis/home)

Verteilung der Scrum Rollen:<br><br>

Scrum Master:  <br>
31.05.2021- 04.06.2021: Tom Bartelsmeier<br>
07.06.2021 - 11.06.2021: Aylin Yildiz<br>
14.06.2021 - 18.06.2021: Patrick Lokau<br>
Aufgabe: Moderiert das Daily Standup, präsentiert der Klasse den aktuellen Entwicklungsstand und kümmert sich um die Organissation (falls es Fragen gibt etc.)<br>

Product Owner:<br>
Aufgabe: Legt fest in welcher Reihenfolge etwas erledigt werden soll bzw. priorisiert die Aufgaben<br>
Annabell Schieke<br>

Team:<br>
Aufgabe: Arbeitet am Produkt<br>
Annabell Schieke, Aylin Yildiz, Patrick Lokau, Paul Goossens, Tom Bartelsmeier<br>
